% Script for the reformulation for the integrated states of the 
% infinitesimal generator in population models with one and two structures
% formulated as first-order hyperbolic PDEs

%% Examples with two physiological structures and trivial velocities

%% Analytic eigenfunctions

%% Ex. 1.1
x=[0, 1]; y=[0, 1]; alpha=@(x, xi, sigma) 1; beta=@(y, xi, sigma) 1;
u=@(x, y) 1; lambda=-1; phi=@(x, y) 1; gx=@(x) 1; gy=@(y) 1;
IG = IG2Str(x, y, gx, gy, alpha, beta, u, 30, 30);

%% Ex. 1.2 
x=[pi/6, pi/2]; y=[pi/6, pi/4]; c=(2^(1/2)/4 - 6^(1/2)/4 + 1/2)^(-1);
alpha=@(x, xi, sigma) c*cos(x-pi/6); beta=@(y, xi, sigma) c*cos(pi/6-y);
u=@(x, y) 1; lambda=-1; phi=@(x, y)  cos(x-y); gx=@(x) 1; gy=@(y) 1;
IG = IG2Str(x, y, gx, gy, alpha, beta, u, 30, 30);

%% Ex. 1.3 
x=[0, 2]; y=[-1, 1]; c=(1/4); alpha=@(x, xi, sigma) c*exp(x-xi+sigma+1);
beta=@(y, xi, sigma) c*exp(-y-xi+sigma); u=@(x, y) 1; lambda=-1; 
phi=@(x, y) exp(x-y); gx=@(x) 1; gy=@(y) 1;
IG = IG2Str(x, y, gx, gy, alpha, beta, u, 30, 30);

%% Ex. 1.4

x=[0, 2]; y=[0, 1]; c= 0.659776652091366; alpha=@(x, xi, sigma) c*exp(-x.^2);
beta=@(y, xi, sigma) c*exp(y); u=@(x, y) (2*x+1); lambda=-2; 
phi=@(x, y) exp(-x.^2+y); gx=@(x) 1; gy=@(y) 1;
IG = IG2Str(x, y, gx, gy, alpha, beta, u, 30, 30);

%% Nonsmooth eigenfunctions

%% Ex. 2.1
x=[0, 1]; y=[0, 2]; alpha=@(x, xi, sigma) (5/8)*(x.^2).*abs(x);
beta=@(y, xi, sigma) (5/8)*(y.^2).*abs(y); u=@(x, y) 1; lambda=-1;
phi=@(x, y) ((x-y).^2).*abs(x-y); gx=@(x) 1; gy=@(y) 1;
IG = IG2Str(x, y, gx, gy, alpha, beta, u, 30, 30);

%% Ex. 2.2
x=[0, 1]; y=[0, 2]; alpha=@(x, xi, sigma) (-6/7)*x.*abs(x);
beta=@(y, xi, sigma) (6/7)*y.*abs(y); u=@(x, y) 1; lambda=-1; 
phi=@(x, y) (x-y).*abs(x-y); gx=@(x) 1; gy=@(y) 1;
IG = IG2Str(x, y, gx, gy, alpha, beta, u, 30, 30);

%% Ex. 2.3
x=[0, 1]; y=[0, 2]; alpha=@(x, xi, sigma) (3/4)*abs(x); 
beta=@(y, xi, sigma) (3/4)*abs(y); u=@(x, y) 1; lambda=-1; 
phi=@(x, y) abs(x-y); gx=@(x) 1; gy=@(y) 1;
IG = IG2Str(x, y, gx, gy, alpha, beta, u, 30, 30);

%% Ex. 2.4 - PcwIG2Str needed
x=[0, 1]; y=[0, 2]; alpha=@(x, xi, sigma) 2*functioncases1(x);
beta=@(y, xi, sigma) 2*functioncases2(y); u=@(x, y) 1; gx=@(x) 1; gy=@(y) 1;
phi=@(x, y)functioncases(x, y); lambda= -1;
IG = PcwIG2Str(x, y, gx, gy, alpha, beta, u, 30, 30);

%% Example with two physiological structures and nontrivial velocities
x=[0.5, 1.5]; y=[0.5, 2]; c=(1/4)*pi*(erf(1/2)-erf(2))*(erfi(1/2)-erfi(3/2)); 
c1=(1/8)*exp(-1/4)/c; c2=(1/2)*exp(1/4)/c; gx=@(x) x; gy=@(y) y.^2/2;
alpha=@(x, xi, sigma) exp(x.^2)*c1; beta=@(y, xi, sigma) exp(-y.^2)*c2;
u=@(x, y) y.^3-2*x.^2-y+4; lambda=-5; phi=@(x, y)  exp(x.^2-y.^2); 
IG = IG2Str(x, y, gx, gy, alpha, beta, u, 30, 30);

%% Examples with one physiological structure
x=[0, 2]; beta=@(x) exp(-x); u=@(x) 1; lambda = -1.203187869979970;
phi=@(x) exp(-x*(1+s));
IG = OneStrIG(x, beta, u, 30);

%% auxiliary functions
function [s]=functioncases(x, y)

if x>=y
 s=1;
else 
 s=0;
end
end

function [s]=functioncases1(x)

if x>=0
 s=1;
else 
 s=0;
end
end

function [s]=functioncases2(y)

if y<=0
 s=1;
else 
 s=0;
end
end