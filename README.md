# IGStr

IGStr is a package for the pseudospectral discretization of the reformulation in terms of the integrated state of the infinitesimal generator of linear population models with  one and two physiological structures formulated as first-order hyperbolic PDEs. 

This repository contains the MATLAB codes for the pseudospectral discretization of the reformulation for the integrated states of the Infinitesimal generator for population models with  one and two physiological structures formulated as first-order hyperbolic PDEs.


# Description of the codes


## OneStrIG

Computes the pseudospectral discretization of the reformulation in terms of the integrated state of the infinitesimal generator of linear population models with  one physiological structure formulated as first-order hyperbolic PDEs. Does not work correctly if piecewise-defined coefficients for the model are taken into account.


## PcwOneStrIG

Computes the pseudospectral discretization of the reformulation in terms of the integrated state of the infinitesimal generator of linear population models with  one physiological structure formulated as first-order hyperbolic PDEs. To be used when piecewise-defined coefficients for the model are taken into account.


## IG2Str

Computes the pseudospectral discretization of the reformulation in terms of the integrated state of the infinitesimal generator of linear population models with two physiological structures formulated as first-order hyperbolic PDEs. Does not work correctly if piecewise-defined coefficients for the model are taken into account.


## PcwIG2Str

Computes the pseudospectral discretization of the reformulation in terms of the integrated state of the infinitesimal generator of linear population models with two physiological structures formulated as first-order hyperbolic PDEs. To be used when piecewise-defined coefficients for the model are taken into account.

-----------

## difmat.m
Computes the differentiation matrix relevant to the Chebysbhev extremal nodes on a given interval

-----------

## flcencurt.m
Computes the Chebyshev extremal nodes and the Clenshaw-Curtis quadrature weights on a given interval.


# Copyright and licensing


## IGStr

Copyright (c) 2021-2022 Simone De Reggi

IGStr is distributed under the terms of the MIT license (see LICENSE.txt).

If you use this software for a scientific publication,
please cite the following publications:

A. Andò, S. De Reggi, D. Liessi, F. Scarabel, A pseudospectral method for investigating the stability of linear population models with two
physiological structures, submitted

-----------

## difmat.m

Copyright (c) 2000 Lloyd N. Trefethen

The code in difmat.m is taken from L. N. Trefethen, Spectral Methods in MATLAB, SIAM, 2000, DOI: 10.1137/1.9780898719598.

Even though the codes therein are not explicitly licensed, considering that they are made available for download on the author’s web page and that 
variations of some of them are included in Chebfun, which is distributed under the 3-clause BSD license, I believe that the author’s intention was 
for his codes to be freely used and that distributing this file together with DoubleStructureR0 does not violate his rights.

-----------

## flcencurt.m

fclencurt, version 1.0.0.0, 02/12/2005, by Greg von Winckel

Copyright (c) 2009, Greg von Winckel

Retrieved on November 17, 2021 from https://www.mathworks.com/matlabcentral/fileexchange/6911-fast-clenshaw-curtis-quadrature

fclencurt is distributed under the terms of the 2-clause BSD license 
