function [IG] = OneStrIG(x, beta, u, N)

% Copyright (c) 2021-2022 Simone De Reggi.
% OneStrIG is distributed under the terms of the MIT license.
%
% If you use this software for a scientific publication,
% please cite the following publication:
%
% [1] A. Andò, S. De Reggi, D. Liessi, F. Scarabel, A pseudospectral method
%     for investigating the stability of linear population models with two
%     physiological structures, submitted
%
% [IG] = OneStrIG(x, beta, u, N) 
% computes the pseudospectral discretization of the reformulation for the
% integrated state of the infinitesimal generator of linear population 
% models with one physiological structures formulated as first-order 
% hyperbolic PDEs according to the the method described in [1] using N+1 
% Chebyshev extremal nodes in the interval  [x(1), x(end)] 
% If piecewise - defined functions are taken into account as parameters for
% the model, consider using PcwOneStrIG for  more accurate results.
%
%  INPUT:
%           x = vectors 
%     beta, u = function of one variable
%           K = function of three variables
%           N = degree of the approximating polynomial
%   
%  OUTPUT:
%          IG = discretized infinitesimal generator
%
%  CALL:
%        >>[IG] = OneStrIG(x, beta, u, 30)

%% Chebyshev nodes and Clenshaw Curtis quadature weights

[xpoint, w]=fclencurt(N+1,x(1),x(end));

%% Differentiation matrix                                                  

D = difmat(xpoint);
D1=D(2:N+1, 2:N+1);
I=D1^(-1);       

%% Creating the matrix BC for the boundary condition                       

%clenshaw-curtis quadrature

BC=(w.*beta(xpoint))*D;
BC=repmat(BC, N+1, 1);
BC=BC(2:N+1, 2:N+1);

%using the inverse of the differentiation matrix
% BC=(I(N, :).*beta(xpoint(2:N+1)))*D1;
% BC=repmat(BC, N, 1);

%% Creating the matrix U                                                   

U=u(xpoint(2:N+1)');
U=U.*D1;
U=I*U;                                                                        

%% Calculating IG                                                          

IG=-D1+BC-U;

end