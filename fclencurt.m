function [x,w]=fclencurt(N,a,b)  

% Copyright (c) 2009, Greg von Winckel
% All rights reserved.
% 
% Computes the N Chebyshev extremal nodes and the relevant weights for the 
% Clenshaw-Curtis Quadrature on the interval [a,b] according to [1]. 
%
%  INPUT:
%          N = number of nodes
%        a,b = extremes of the interval 
%  OUTPUT:
%          x =  vector containgn N-Chebyshev extremal nodes in [a, b] 
%          w =  vector containing the weights for the Clenshaw-Curtis 
%                quadrature associated to the nodes in x
%  CALL:
%        >>[x,w]=fclencurt(N,a,b) ;
%
% REFERENCES: 
%            [1] Greg von Winckel (2021). Fast Clenshaw-Curtis Quadrature
%                (https://www.mathworks.com/matlabcentral/fileexchange/
%                6911-fast-clenshaw-curtis-quadrature), MATLAB Central 
%                File Exchange. Retrieved November 16, 2021.

n=N-1; bma=b-a;
c=zeros(N,2);
c(1:2:N,1)=(2./[1 1-(2:2:n).^2 ])'; c(2,2)=1;
f=real(ifft([c(1:N,:);c(n:-1:2,:)]));
w=bma*([f(1,1); 2*f(2:n,1); f(N,1)])/2;
x=0.5*((b+a)+n*bma*f(1:N,2));
x=flip(x)';
w=flip(w)';

end
