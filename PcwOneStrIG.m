function [IG] = PcwOneStrIG(x, beta, u, N)

% Copyright (c) 2021-2022 Simone De Reggi.
% PcwOneStrIG is distributed under the terms of the MIT license.
%
% If you use this software for a scientific publication,
% please cite the following publication:
%
% [1] A. Andò, S. De Reggi, D. Liessi, F. Scarabel, A pseudospectral method
%     for investigating the stability of linear population models with two
%     physiological structures, submitted
%
% [IG] = PcwOneStrIG(x, beta, u, N)
% computes the pseudospectral discretization of the reformulation in terms
% of the integrated states of the infinitesimal generator of linear population 
% models with one physiological structures formulated as first-order 
% hyperbolic PDEs according to the the method described in [1] using N+1 
% Chebyshev extremal nodes in the interval  [x(1), x(end)] 
%
%  INPUT:
%           x = vectors 
%     beta, u = function of one variable
%           N = degree of the approximating polynomial
%   
%  OUTPUT:
%          IG = discretized infinitesimal generator
%
%  CALL:
%        >>[IG] = PcwOneStrIG(x, beta, u, 30)

%% Chebyshev nodes and Clenshaw Curtis quadature weights

[xpoint, w]=fclencurt(N+1,x(1),x(end));

%% Differentiation matrix                                                  

D = difmat(xpoint);
D1=D(2:N+1, 2:N+1);
D2=D1^(-1);       

%% Creating the matrix BC for the boundary condition                       

%clenshaw-curtis quadrature
b=zeros(N+1, 1);
for i=1:N+1
  b(i)=beta(xpoint(i));
end
BC=(w.*b)*D;
BC=repmat(BC, N+1, 1);
BC=BC(2:N+1, 2:N+1);

% using the inverse of the differentiation matrix
% BC=(D2(N, :).*beta(xpoint(2:N+1))')*D1;
% BC=repmat(BC, N, 1);

%% Creating the matrix U                                                   

U=zeros(N, 1);
for i=2:N+1
  U(i-1)=u(xpoint(i));
end
U=U.*D1;
U=D2*U;                                                                        

%% Calculating IG                                                          

IG=-D1+BC-U;

end
