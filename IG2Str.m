function [IG] = IG2Str(x, y, gx, gy, alpha, beta, u, N, M)

% Copyright (c) 2021-2022 Simone De Reggi.
% IG2Str is distributed under the terms of the MIT license.
%
% If you use this software for a scientific publication,
% please cite the following publication:
%
% [1] A. Andò, S. De Reggi, D. Liessi, F. Scarabel, A pseudospectral method
%     for investigating the stability of linear population models with two
%     physiological structures, submitted
%
% [IG] = IG2Str(x, y, gx, gy, alpha, beta, u, N, M)
% computes the pseudospectral discretization of the reformulation in terms
% of the integrated state of the infinitesimal generator of linear population 
% models with two physiological structures formulated as first-order 
% hyperbolic PDEs according to the the method described in [1] using N+1 
% Chebyshev extremal nodes in the interval  [x(1), x(end)] 
% and M+1 Chebyshev extremal nodes in the interval [y(1), y(end)]. 
% If piecewise - defined functions are taken into account as parameters 
% for the model, consider using PcwIG2Str for more accurate results.
%
%  INPUT:
%         x,y = vectors 
%       gx,gy = functions of one variable 
% alpha, beta = functions of three variables
%           u = function of two variables  
%           N = degree of the approximating polynomial in [x(1), x(end)]
%           M = degree of the approximating polynomial in [y(1), y(end)]
%  
%  OUTPUT:
%          IG = discretized infinitesimal generator
%
%  CALL:
%        >>[IG] = IG2Str(x, y, gx, gy, alpha, beta, u, 30, 30)

%% Chebyshev nodes and Clenshaw-Curtis quadrature weights

[xpoint, w] = fclencurt(N+1,x(1),x(end));
[ypoint, v] = fclencurt(M+1,y(1),y(end));

weights=kron(w, v);

%% Differentiation matrices                   

[X, Y]=meshgrid(xpoint(2:N+1), ypoint(2:M+1));
[XX, YY]=meshgrid(xpoint, ypoint);

X1=reshape(X, [], 1);
Y1=reshape(Y, [], 1);
XX1=reshape(XX, [], 1);
YY1=reshape(YY, [], 1);

DL1 = difmat(xpoint);
DL2 = difmat(ypoint);

D=kron(DL1(2:N+1, 2:N+1), DL2(2:M+1, 2:M+1));
D1=kron(DL1, DL2);

%% Inverses of differentiation matrices                   

I1=DL1(2:N+1, 2:N+1)^(-1);
I1=kron(I1, eye(M));           %inverse of the differentiation matrix in x

I2=DL2(2:M+1, 2:M+1)^(-1);
I2=kron(eye(N), I2);           %inverse of the differentiation matrix in y

%% Differentiation matrices kron                   

DL1=kron(DL1(2:N+1, 2:N+1), eye(M)); 
Gx=gx(X1);           
L=Gx.*DL1;
  
DL2=kron(eye(N), DL2(2:M+1, 2:M+1));      
Gy=gy(Y1);             
G=Gy.*DL2;

%% Index position

e=ones(1, (N+1)*(M+1));

for i=1:(N+1)*(M+1)
  if mod(i,(M+1))==1 || (i>0 && i<=M+1)
    e(i)=0;
  else
    e(i)=i;
  end
end
e=find(e);

%% Matrix BC1 for the boundary condition in x                 

A=alpha(XX1, XX1', YY1');  %for nonpiecewise functions
[s1, s2]=size(A); 
if s1==1
  A=repmat(A, (N+1)*(M+1), 1);
end
if s2==1
  A=repmat(A, 1, (N+1)*(M+1));
end

A=(weights.*A)*D1;
A=A(e, e);
BC1=I1*A;

%% Matrix BC2 for the boundary condition in y                 

B=beta(YY1, XX1', YY1');   
[s1, s2]=size(B);
if s1==1
  B=repmat(B, (N+1)*(M+1), 1);
end
if s2==1
  B=repmat(B, 1, (N+1)*(M+1));
end

B=(weights.*B)*D1;            
B=B(e, e);
BC2=I2*B;
                                                                         
%% Creating the matrix U                                                   

U=u(X1, Y1);                 %for nonpiecewise functions
D=U.*D;
U=I1*I2*D;
              
%% Computing A                                                          

IG=-L-G+BC1+BC2-U;

end
